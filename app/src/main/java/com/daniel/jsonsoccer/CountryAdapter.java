package com.daniel.jsonsoccer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class CountryAdapter extends ArrayAdapter<CountrySpinner> {

    public CountryAdapter(@NonNull Context context, ArrayList<CountrySpinner> countrySpinnerArrayList){
        super(context, 0, countrySpinnerArrayList);

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position,convertView,parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position,convertView,parent);
    }

    private View initView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.spinner_row, parent, false
            );
        }
        ImageView imageView = convertView.findViewById(R.id.imageSpinner);
        TextView textView = convertView.findViewById(R.id.textSpinner);
        CountrySpinner currentItem = getItem(position);
        if (currentItem != null) {
            imageView.setImageResource(currentItem.getImageSpinner());
            textView.setText(currentItem.getTextSpinner());
        }
        return convertView;
    }

}

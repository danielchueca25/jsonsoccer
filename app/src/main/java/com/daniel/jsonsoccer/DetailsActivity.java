package com.daniel.jsonsoccer;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DetailsActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<Sports> sports = new ArrayList<>();
    MyAdapter myAdapter;

    static  String JSON = "https://www.thesportsdb.com/api/v1/json/2/search_all_leagues.php?c=";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deatils);
        recyclerView = findViewById(R.id.recyclerView);
        JSON = "https://www.thesportsdb.com/api/v1/json/2/search_all_leagues.php?c=";
        String pais ="";
        pais = getIntent().getStringExtra("pais");
        JSON = JSON +pais;
        getSports();



    }

    private void getSports() {

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                JSON ,
                null,
                (Response.Listener<JSONObject>) response -> {

                        try {
                           JSONArray jsonArray =  response.getJSONArray("countrys");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject sportObject = jsonArray.getJSONObject(i);
                                Sports sport = new Sports();
                                sport.setFlag(sportObject.getString("strBadge"));
                                sport.setTextTitle(sportObject.getString("strLeague"));
                                sport.setTextDescription(sportObject.getString("strDescriptionEN"));
                                sport.setWebUrl(sportObject.getString("strWebsite"));
                                ArrayList<String> images = new ArrayList<>();
                                images.add(sportObject.getString("strFanart1"));
                                images.add(sportObject.getString("strFanart2"));
                                images.add(sportObject.getString("strFanart3"));
                                images.add(sportObject.getString("strFanart4"));
                                sport.setImages(images);
                                sports.add(sport);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    recyclerView.setLayoutManager(new
                            LinearLayoutManager(getApplicationContext()));
                    myAdapter = new MyAdapter(sports,getApplicationContext());
                    recyclerView.setAdapter(myAdapter);
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("tag","onErrorResponse: " + error.getMessage());
                    }
                }

        );

        queue.add(jsonObjectRequest);
    }
}
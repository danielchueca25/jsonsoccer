package com.daniel.jsonsoccer;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;

public class ViewPagerActivity extends AppCompatActivity {
    ViewPager viewPager;
    ArrayList<String> images;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);
        viewPager = findViewById(R.id.viewPager);
        images = getIntent().getStringArrayListExtra("images");
        ImageAdapter imageAdapter = new ImageAdapter(getApplicationContext(),images);
        viewPager.setAdapter(imageAdapter);
    }
}
package com.daniel.jsonsoccer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Spinner spinnerClass;
    private ArrayList<CountrySpinner> countrySpinnerArrayList;
    private CountryAdapter countryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinnerClass = findViewById(R.id.spinnerClass);
        initList();

        countryAdapter = new CountryAdapter(getApplicationContext(), countrySpinnerArrayList);

        spinnerClass.setAdapter(countryAdapter);
        spinnerClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountrySpinner selectedItem = (CountrySpinner) parent.getItemAtPosition(position);
                String selectedItemTextSpinner = selectedItem.getTextSpinner();
                if (!selectedItemTextSpinner.equals("Select a country")) {
                    Intent intent = new Intent(MainActivity.this, DetailsActivity.class);

                    intent.putExtra("pais", selectedItemTextSpinner);
                    startActivity(intent);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(MainActivity.this, "Nothing selected", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initList(){
        countrySpinnerArrayList = new ArrayList<>();
        countrySpinnerArrayList.add(new CountrySpinner("Select a country", 0));
        countrySpinnerArrayList.add(new CountrySpinner("Argentina",R.drawable.argentina));
        countrySpinnerArrayList.add(new CountrySpinner("Australia",R.drawable.australia));
        countrySpinnerArrayList.add(new CountrySpinner("Canada",R.drawable.canada));
        countrySpinnerArrayList.add(new CountrySpinner("Congo",R.drawable.congo));
        countrySpinnerArrayList.add(new CountrySpinner("Egypt",R.drawable.egypt));
        countrySpinnerArrayList.add(new CountrySpinner("France",R.drawable.france));
        countrySpinnerArrayList.add(new CountrySpinner("Germany",R.drawable.germany));
        countrySpinnerArrayList.add(new CountrySpinner("Greece",R.drawable.greece));
        countrySpinnerArrayList.add(new CountrySpinner("India",R.drawable.india));
        countrySpinnerArrayList.add(new CountrySpinner("Indonesia",R.drawable.indonesia));
        countrySpinnerArrayList.add(new CountrySpinner("Ireland",R.drawable.ireland));
        countrySpinnerArrayList.add(new CountrySpinner("Italy",R.drawable.italy));
        countrySpinnerArrayList.add(new CountrySpinner("Kenya",R.drawable.kenya));
        countrySpinnerArrayList.add(new CountrySpinner("Mexico",R.drawable.mexico));
        countrySpinnerArrayList.add(new CountrySpinner("New Zealand",R.drawable.newzeland));
        countrySpinnerArrayList.add(new CountrySpinner("Norway",R.drawable.norway));
        countrySpinnerArrayList.add(new CountrySpinner("Poland",R.drawable.poland));
        countrySpinnerArrayList.add(new CountrySpinner("Qatar",R.drawable.qatar));
        countrySpinnerArrayList.add(new CountrySpinner("SouthAfrica",R.drawable.southafrica));
        countrySpinnerArrayList.add(new CountrySpinner("SouthKorea",R.drawable.southkorea));
        countrySpinnerArrayList.add(new CountrySpinner("Spain",R.drawable.spain));
        countrySpinnerArrayList.add(new CountrySpinner("United Kingdom",R.drawable.uk));
        countrySpinnerArrayList.add(new CountrySpinner("United States",R.drawable.usa));


    }
}